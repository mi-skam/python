with import <nixpkgs> {};

( let
    axelrod = python36.pkgs.buildPythonPackage rec {
      pname = "Axelrod";
      version = "4.1.0";

      src = python36.pkgs.fetchPypi {
        inherit pname version;
        sha256 = "1d5770m8xcd0lhqaajxrvym0v5nxy05hbgsbi82q31s7r9420464";
      };

      propagatedBuildInputs = with pkgs.python36Packages; [ cloudpickle
                                                            dask
                                                            matplotlib
                                                            prompt_toolkit
                                                            scipy
                                                            tqdm
                                                            toolz ];

      doCheck = false;

      meta = {
        homepage = "http://axelrod.readthedocs.org/";
        description = "Reproduce the Axelrod iterated prisoners dilemma tournament";
      };
    };

  in python36.withPackages (ps: [ps.ipython axelrod])
).env