def get_numerals(number):
    '''returns a list of mantisses for numbers base 10,
       from most to least significant digits.
    '''
    exp = len(str(number)) - 1
    result = []
    while number > 1:
        result.append(number // 10 ** exp)
        number = number % 10 ** exp
        exp -= 1
    return result


print(get_numerals(1234))
