#! /usr/bin/env nix-shell
#! nix-shell -i 'python3.withPackages(ps: [ps.numpy])'

import numpy

print(numpy.__version__)
