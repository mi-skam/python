def map_wrapper(f):
    def collector(lst):
        return map(f, lst)
    return collector


def binary_wrapper(func):
    def f(irst):
        def s(econd):
            return func(irst, econd)
        return f


#wrapper(map)(lambda x: x**2, [1, 2, 3])

binary_wrapper(map)(lambda x: x**2)([1, 2, 3])
