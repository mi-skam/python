import math
import sys


def zeros(n):
    k_max = math.floor(math.log(n, 5))
    sum = 0
    for k in range(1, k_max + 1):
        sum += math.floor(n/5**k)
    return sum


if __name__ == '__main__':
    print(zeros(int(sys.argv[1])))
