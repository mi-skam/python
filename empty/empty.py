def hello(name):
    print(f"Hello {name}")

def main():
    name = input("What is your name?").strip()
    hello(name)

if __name__ == '__main__':
    main()